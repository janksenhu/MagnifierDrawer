package cc.soga.drawertest;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Properties;


import android.util.Log;

/**
 * 
 * <b>类描述：</b> 自定义一个Log类，发布的时候只�?��设置Debug为false就不会再打印log�?<br/> 
 * <b>创建人：</b>janksenhu(QQ:799423779)  	<br/>  
 * <b>修改时间�?/b>2013-6-27 下午7:26:19	<br/>
 * @version 1.0.0	<br/>  
 *
 */
public class ILogUtil {
	private static final String tag = ILogUtil.class.getSimpleName();
	
	//如果是Debug版本，全部打印日�?
	public static final boolean DEBUG = false;
	//过滤关键字，如果包含下列关键字的则不打印
	public static final HashSet<String> ignorTag = new HashSet<String>();
	
	public static boolean allowDebug(){
		return DEBUG;
	}
	
	static{
//		ignorTag.add("Asyntask");
//		ignorTag.add("AAAAAAAAAA");
//		ignorTag.add("Connection is ");
//		ignorTag.add("Connection install ");
//		
//		ignorTag.add("checkFile");
//		ignorTag.add("AsyntaskHour");
//		ignorTag.add("CheckUid");
//		ignorTag.add("Asyntasklimit");
//		ignorTag.add("AsyntaskJson");
//		ignorTag.add("ActivityThread");
//
//		ignorTag.add("findUin");
//		ignorTag.add("updateDevice");
//		ignorTag.add("ConfigUtil");
		
//		ignorTag.add("uploadLog001");
//		ignorTag.add("Reg");
//		ignorTag.add("deviceId");
//		ignorTag.add("updateDeviceA");
//		ignorTag.add("updateGGG");
//		ignorTag.add("updateDevice");
//		ignorTag.add("UploadResc");
//		ignorTag.add("findUinFinalli");
//		ignorTag.add("ActivityThread");
	}
	
	public static void v(String tag, String msg) {
		if (allowDebug() && !ignorTag.contains(tag)) {
			Log.v(tag, msg);
		}
	}

	public static void d(String tag, String msg) {
		if (allowDebug()  && !ignorTag.contains(tag)) {
			Log.d(tag, msg);
		}
	}

	public static void i(String tag, String msg) {
		if (allowDebug()  && !ignorTag.contains(tag)) {
			Log.i(tag, msg);
		}
	}

	public static void w(String tag, String msg) {
		if (allowDebug()  && !ignorTag.contains(tag)) {
			Log.w(tag, msg);
		}
	}

	public static void e(String tag, String msg) {
		if (allowDebug()  && !ignorTag.contains(tag)) {
			Log.e(tag, msg);
		}
	}
	
	/**
	 * 
	 * <b>方法描述�?/b>可以调用打印异常信息到logcat	<br/>  
	 * @param tag
	 * @param exc   
	 * @exception	<br/> 
	 * @since  1.0.0
	 */
	public static void exception(String tag,Exception e) {
		if (allowDebug()  && !ignorTag.contains(tag)) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			Log.e(tag, sw.toString());
		}
	}
	
	/**
	 * 获取异常的堆栈信�?
	 * @param e
	 * @return
	 */
	public static String getMessageStrOfException(Exception e){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}
	
	
	
}
