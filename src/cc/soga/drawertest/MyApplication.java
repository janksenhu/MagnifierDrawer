package cc.soga.drawertest;

import android.app.Application;
import android.content.Context;


public class MyApplication extends Application {
	private static final String tag = MyApplication.class.getSimpleName();
	public static Context context = null ;
	
	
	@Override
    public void onCreate() {
        // The following line triggers the initialization of ACRA
			
        super.onCreate();
        if(context==null){	
        	context = getApplicationContext();
    	}
    }
	
	
}
