package cc.soga.drawertest;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class SuspensionLayerService extends Service{
	private static final String tag =SuspensionLayerService.class.getSimpleName();
	
	// 定义浮动窗口布局
	private LayoutInflater inflater = LayoutInflater.from(MyApplication.context);
	private RelativeLayout mFloatLayout = (RelativeLayout) inflater.inflate(R.layout.suspend_layer, null);// 获取浮动窗口视图�?��布局;
	private WindowManager.LayoutParams wmParams = new WindowManager.LayoutParams();;
	private WindowManager mWindowManager = (WindowManager) MyApplication.context.getSystemService(getApplication().WINDOW_SERVICE) ;// 创建浮动窗口设置布局参数的对�?

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		ILogUtil.i("SuspensionLayerService", "notification");
	

		
		createFloatView();
		
		
		//防止被杀，发送空消息
//		Notification notification = new Notification(R.drawable.ic_launcher, "interflow test service is running",System.currentTimeMillis());
//		Intent notificationIntent = new Intent();
//		PendingIntent pintent = PendingIntent.getService(this, 0, notificationIntent, 0);
//        notification.setLatestEventInfo(this, "Interflow Test Service","interflow test service is running�?, pintent);
//        startForeground(startId, notification);
		
		
        
		return super.onStartCommand(intent, flags, startId);
	}


	/**
	 * 
	 * <b>方法描述�?/b>显示悬浮窗口 <br/>
	 * 
	 * @exception <br/>
	 * @since 1.0.0
	 */
	@SuppressLint("NewApi")
	private void createFloatView() {
		wmParams.type = LayoutParams.TYPE_PHONE;// 设置window type
		wmParams.format = PixelFormat.RGBA_8888;// 设置图片格式，效果为背景透明
		wmParams.flags = LayoutParams.FLAG_NOT_FOCUSABLE;// 设置浮动窗口不可聚焦（实现操作除浮动窗口外的其他可见窗口的操作）
		wmParams.gravity = Gravity.LEFT | Gravity.TOP;// 调整悬浮窗显示的停靠位置为左侧置�?
		wmParams.x = 0;// 以屏幕左上角为原点，设置x、y初始值，相对于gravity
		wmParams.y = 0;

		// 设置悬浮窗口长宽数据
		wmParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
		wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

		mWindowManager.addView(mFloatLayout, wmParams);// 添加mFloatLayout
		
		mFloatLayout.measure(View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

		// 设置监听浮动窗口的触摸移�?
		mFloatLayout.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) { 
				// TODO Auto-generated method stub
				//getRawX是触摸位置相对于屏幕的坐标，getX是相对于按钮的坐�?
				wmParams.x = (int) arg1.getRawX() - mFloatLayout.getMeasuredWidth()/2;
				ILogUtil.i(tag, "RawX" + arg1.getRawX());
				ILogUtil.i(tag, "X" + arg1.getX());
				//�?5为状态栏的高�?
	            wmParams.y = (int) arg1.getRawY() - mFloatLayout.getMeasuredHeight()/2 - 25;
	            ILogUtil.i(tag, "RawY" + arg1.getRawY());
	            ILogUtil.i(tag, "Y" + arg1.getY());
	             //刷新
	            mWindowManager.updateViewLayout(mFloatLayout, wmParams);
				
				return false;
			}
		});


	}

	/**
     * 关闭浮动显示对象
     */
    private void closeFloatView() {
        if (mFloatLayout != null) {
        	mWindowManager.removeView(mFloatLayout);
        }
    }

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void onDestroy() {
	
		
	}	
}
